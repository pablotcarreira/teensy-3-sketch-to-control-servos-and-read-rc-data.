// Do not remove the include below
#include "EstacaoMeteo.h"

/*
Preparação do Display.
pin 1 - Serial clock out (SCLK)
pin 2 - Serial data out (DIN)
pin 3 - Data/Command select (D/C)
pin 4 - LCD chip select (CS)
pin 5 - LCD reset (RST)
*/
Adafruit_PCD8544 display = Adafruit_PCD8544(1, 2, 3, 4, 5);
//#define LOGO16_GLCD_HEIGHT 16
//#define LOGO16_GLCD_WIDTH  16


/*
 * Modos:
 * 0 = Tela principal
 * 1 = Mostra A
 * 2 = Mostra B
 * 3 = Mostra C
 * 4 = Mostra D
*/
int modo = 0;

// AM2301 (sensor de umidade e temperatura).
DHT dht;

// BMP085 (sensor de pressao e temperatura).
Adafruit_BMP085 bmp;

bool bmp_ok = false;
bool am2301_ok = false;

void setup()   {
	// Saida serial.
	Serial.begin(9600);

	// Prepara o display.
	display.begin();
	display.setContrast(60);
	display.setCursor(0,0);
	display.print("V 1.05");
	display.display();
	delay(3000);

	// Prepara os sensores.
	dht.setup(6, dht.AM2302);
	am2301_ok = true;
//
//	if (!bmp.begin()) {
//		display.clearDisplay();
//		display.setCursor(0,0);
//		display.print("Erro, sensor pressao");
//		bmp_ok = false;
//		display.display();
//		delay(1000);
//	  }


}

void loop() {
  switch(modo){
    case 0:
      printStatus();
//      Serial.print("Temperature = ");
//	  Serial.print(bmp.readTemperature());
//	  Serial.println(" *C");
      break;
    case 1:
      break;
  }
  delay(1000);
}

void printStatus(){
	// Limpa e configura o display.
    display.clearDisplay();
    display.setCursor(0,0);
    display.setTextColor(BLACK);
    display.setTextSize(2);

    // Temperatura.
    display.print("T:");
    display.print(dht.getTemperature());

    // Pressão.
//    display.print("P:");
//    display.print(bmp.readPressure());

    // Umidade.
    display.print("U:");
    display.print(dht.getHumidity());

    // Mostra os dados.
    display.display();
}

