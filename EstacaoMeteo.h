// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef _EstacaoMeteo_H_
#define _EstacaoMeteo_H_
#include "Arduino.h"

//add your includes for the project EstacaoMeteo here
#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <DHT.h>
#include <Adafruit_BMP085.h>
//end of add your includes here

#ifdef __cplusplus
extern "C" {
#endif
void loop();
void setup();
#ifdef __cplusplus
} // extern "C"
#endif

//add your function definitions for the project EstacaoMeteo here
void printStatus();




//Do not add code below this line
#endif /* _EstacaoMeteo_H_ */
